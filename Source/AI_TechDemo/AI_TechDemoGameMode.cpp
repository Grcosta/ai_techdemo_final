// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "AI_TechDemoGameMode.h"
#include "AI_TechDemoCharacter.h"
#include "UObject/ConstructorHelpers.h"

AAI_TechDemoGameMode::AAI_TechDemoGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
