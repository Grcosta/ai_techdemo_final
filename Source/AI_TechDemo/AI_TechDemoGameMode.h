// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "AI_TechDemoGameMode.generated.h"

UCLASS(minimalapi)
class AAI_TechDemoGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AAI_TechDemoGameMode();
};



